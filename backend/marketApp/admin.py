from django.contrib import admin

from .models.user import User
from .models.product import Product

admin.site.register(User)
admin.site.register(Product)
