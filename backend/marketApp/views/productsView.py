# from rest_framework import views, status
# from marketApp.models.product import Product
# from rest_framework.response import Response
# from marketApp.serializers.productSerializer import ProductSerializerView, ProductSerializer
# from marketApp.models.user import User

from django.conf import settings
from rest_framework import generics, status, views
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend

from marketApp.serializers.userSerializer import UserSerializer
from marketApp.models.user import User
from marketApp.models.product import Product
from marketApp.serializers.productSerializer import ProductSerializer, ProductSerializerView, ProductSerializerAdd

class ProductsView(views.APIView):
    def get(self, request, *args, **kwargs):
        all_products = Product.objects.all()
        serializer = ProductSerializerView(all_products, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data_json = request.data
        serializer = ProductSerializer(data=data_json)
        if serializer.is_valid():
            serializer.save()
            return Response({"menssage" : "Product created"}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ProductsCreateView(generics.CreateAPIView):
    # queryset = User.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (IsAuthenticated, )


    def post(self, request, *args, **kwargs):
        print("***********************************")
        print("Request", request.data)
        print("Args", args)
        print("kwargs", kwargs)
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        token_backend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = token_backend.decode(token, verify=False)

        if valid_data['user_id'] != request.data['user_id']:
            string_response = {'detail' : 'Acceso no autorizado'}
            return Response(string_response, status=status.HTTP_401_UNAUTHORIZED)
        
        
        serializer = ProductSerializer(data=request.data['product_data'])
        serializer.is_valid(raise_exception=True)
        print(User.objects.get(id=request.data['user_id']))
        user_id = User.objects.get(id=request.data['user_id'])
        serializer.save(user=user_id)
                
        return Response({"message" : "All was OK"}, status=status.HTTP_201_CREATED)

class ProductsUpdateView(generics.UpdateAPIView):
    serializer_class = ProductSerializerAdd
    permission_classes = (IsAuthenticated,)
    queryset = Product.objects.all()

    def put(self, request, *args, **kwargs):
        print("***********************************")
        print("Request", request.data)
        print("Args", args)
        print("kwargs", kwargs)
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        token_backend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = token_backend.decode(token, verify=False)

        if valid_data['user_id'] != kwargs['user']:
            string_response = {'detail' : 'Acceso no autorizado'}
            return Response(string_response, status=status.HTTP_401_UNAUTHORIZED)

        request.data['user']=kwargs['user']
        return super().update(request, *args, **kwargs)

class ProductsDeleteView(generics.DestroyAPIView):
    serializer_class = ProductSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Product.objects.all()

    def delete(self, request, *args, **kwargs):
        print("***********************************")
        print("Request", request.data)
        print("Args", args)
        print("kwargs", kwargs)
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        token_backend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = token_backend.decode(token, verify=False)

        if valid_data['user_id'] != kwargs['user']:
            string_response = {'detail' : 'Acceso no autorizado'}
            return Response(string_response, status=status.HTTP_401_UNAUTHORIZED)

        return super().destroy(request, *args, **kwargs)

class ProductsFilterView(generics.ListAPIView):
    serializer_class = ProductSerializer
    
    def get_queryset(self):
        # queryset = super(CLASS_NAME, self).get_queryset()
        # queryset = queryset # TODO
        # return queryset(self):
        queryset = Product.objects.filter(category=self.kwargs['category'])
        print(queryset.all())
        return queryset