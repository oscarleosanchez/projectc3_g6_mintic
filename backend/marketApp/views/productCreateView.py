from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend

from marketApp.serializers.userSerializer import UserSerializer
from marketApp.models.user import User
from marketApp.models.product import Product
from marketApp.serializers.productSerializer import ProductSerializer


class ProductCreateView(generics.CreateAPIView):
    # queryset = User.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (IsAuthenticated, )


    def post(self, request, *args, **kwargs):
        print("***********************************")
        print("Request", request.data)
        print("Args", args)
        print("kwargs", kwargs)
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        token_backend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = token_backend.decode(token, verify=False)

        if valid_data['user_id'] != request.data['user_id']:
            string_response = {'detail' : 'Acceso no autorizado'}
            return Response(string_response, status=status.HTTP_401_UNAUTHORIZED)
        
        
        serializer = ProductSerializer(data=request.data['product_data'])
        serializer.is_valid(raise_exception=True)
        print(User.objects.get(id=request.data['user_id']))
        user_id = User.objects.get(id=request.data['user_id'])
        serializer.save(user=user_id)
                
        return Response({"message" : "All was OK"}, status=status.HTTP_201_CREATED)

class ProductUpdateView(generics.UpdateAPIView):
    serializer_class = ProductSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Product.objects.all()

    def get(self, request, *args, **kwargs):
        print("***********************************")
        print("Request", request.data)
        print("Args", args)
        print("kwargs", kwargs)
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        token_backend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = token_backend.decode(token, verify=False)

        if valid_data['user_id'] != request.data['user_id']:
            string_response = {'detail' : 'Acceso no autorizado'}
            return Response(string_response, status=status.HTTP_401_UNAUTHORIZED)

        return super().update(request, *args, **kwargs)

class ProductDeleteView(generics.DestroyAPIView):
    serializer_class = ProductSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Product.objects.all()

    def get(self, request, *args, **kwargs):
        print("***********************************")
        print("Request", request.data)
        print("Args", args)
        print("kwargs", kwargs)
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        token_backend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = token_backend.decode(token, verify=False)

        if valid_data['user_id'] != request.data['user_id']:
            string_response = {'detail' : 'Acceso no autorizado'}
            return Response(string_response, status=status.HTTP_401_UNAUTHORIZED)

        return super().destroy(request, *args, **kwargs)

class ProductFilterView(generics.ListAPIView):
    serializer_class = ProductSerializer
    
    def get(self):
        queryset = Product.objects.filter(category=self.kwargs['category'])
        return queryset
