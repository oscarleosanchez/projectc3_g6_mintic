from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
# from .productsView import ProductsView
from .productsView import ProductsView, ProductsCreateView, ProductsUpdateView, ProductsDeleteView, ProductsFilterView