from django.db import models
from .user import User

class Product(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name="products", on_delete=models.CASCADE)
    productName = models.CharField(max_length=100)
    description = models.TextField(max_length=255)
    price = models.IntegerField(default=0)
    category = models.CharField(max_length=20)
    datePublished = models.DateTimeField(auto_now_add=True, blank=True)
    location = models.CharField(max_length=100)