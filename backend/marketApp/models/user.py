from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password
from phonenumber_field.modelfields import PhoneNumberField

class UserManager(BaseUserManager):
    def create_user(self, username, password):
        if not username:
            raise ValueError('El usuario debe tener un username')
        
        user  = self.model(username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        user = self.create_user(username, password)
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    id = models.AutoField(primary_key=True)
    username = models.CharField('Username', max_length=20, unique=True)
    password = models.CharField('Password', max_length=255)
    name = models.CharField('Name', max_length=50)
    email = models.EmailField('Email', max_length=100)
    phoneNumber = models.CharField('Phone Number', max_length=10)

    def save(self, **kwargs):
        salt = 'mMUj0DrIK6vgtdIYepkIxN'
        self.password = make_password(self.password, salt)
        super().save(**kwargs)

    objects = UserManager()
    USERNAME_FIELD = 'username'
