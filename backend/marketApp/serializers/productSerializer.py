from marketApp.models.product import Product
from marketApp.models.user import User
from rest_framework import serializers

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['productName', 'description', 'price', 'category', 'datePublished', 'location']

    def to_representation(self, obj):
        user = User.objects.get(pk=obj.pk)
        product = Product.objects.get(id=obj.id)
        print("Obj ",obj)
        
        return {
            "id" : product.id,
            "productName" : product.productName,
            "description" : product.description,
            "price" : product.price,
            "category" : product.category,
            "datePublished" : product.datePublished,
            "location" : product.location,
            "user" : {
                "name" : user.name,
                "email" : user.email,
                "phoneNumber" : user.phoneNumber
            }
        }

    
class ProductSerializerView(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['productName', 'description', 'price']

class ProductSerializerAdd(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['user','productName', 'description', 'price', 'category', 'datePublished', 'location']