from marketApp.models.user import User
from marketApp.models.product import Product
from rest_framework import serializers
from marketApp.serializers.productSerializer import ProductSerializer

class UserSerializer(serializers.ModelSerializer):
    product = ProductSerializer()
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'email', 'phoneNumber', 'product']

    def create(self, validated_data):
        productData = validated_data.pop('product')
        userInstance = User.objects.create(**validated_data)
        
        # Product.objects.create(user=userInstance, **productData)
        if len(productData) != 0:
            Product.objects.create(user=userInstance, **productData)
        return userInstance

    def to_representation(self,obj):
        user = User.objects.get(id=obj.id)
        product = Product.objects.get(user=obj.id)

        return {
            "id" : user.id,
            "username" : user.username,
            "name" : user.name,
            "email" : user.email,
            "phoneNumber" : user.phoneNumber,
            "product" : {
                "id" : product.id,
                "productName" : product.productName,
                "description" : product.description,
                "price" : product.price,
                "category" : product.category,
                "datePublished" : product.datePublished,
                "location" : product.location
            }
        }