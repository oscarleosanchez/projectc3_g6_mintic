
from django.contrib import admin
from django.urls import path

from marketApp import views
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('product/all/', views.ProductsView.as_view()),
    path('product/create/', views.ProductsCreateView.as_view()),
    path('product/delete/<int:user>/<int:pk>/', views.ProductsDeleteView.as_view()),
    path('product/<str:category>/', views.ProductsFilterView.as_view()),
    path('product/update/<int:user>/<int:pk>/', views.ProductsUpdateView.as_view()),
]
